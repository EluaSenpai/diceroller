package cat.tib.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final Random RANDOM = new Random();
    private Button roll, reset;
    private ImageView dau1, dau2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dau1 = findViewById(R.id.dice1);
        dau2 = findViewById(R.id.dice2);

        roll = findViewById(R.id.roll);
        reset=findViewById(R.id.reset);

        final Context context = this;

        reset.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                dau1.setVisibility(View.INVISIBLE);
                dau2.setVisibility(View.INVISIBLE);
                roll.setText("ROLL THE DICE!");
                roll.setTextSize(20);
                roll.setTextColor(0xffffffff);
                roll.setBackgroundColor(0xFF800080);
            }
        });

        roll.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                dau1.setVisibility(View.VISIBLE);
                dau2.setVisibility(View.VISIBLE);
                Toast toast = Toast.makeText(context, "Button pressed", Toast.LENGTH_SHORT);
                toast.show();
                roll.setText("Dice Rolled");
                roll.setTextColor(0xFF000000);
                roll.setTextSize(30);
                roll.setBackgroundColor(0xFFFF0000);

                int valor1 = dice();
                int valor2 = dice();


                switch (valor1) {
                    case 1:
                        dau1.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dau1.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dau1.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dau1.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dau1.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dau1.setImageResource(R.drawable.dice_6);
                        break;
                }

                switch (valor2) {
                    case 1:
                        dau2.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dau2.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dau2.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dau2.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dau2.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dau2.setImageResource(R.drawable.dice_6);
                        break;
                }
                if (valor1 == 6 && valor2 == 6) {
                    Toast jackpot = Toast.makeText(context, "JACKPOT!", Toast.LENGTH_SHORT);
                    jackpot.setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL, 50, 50);
                    jackpot.show();
                }

            }
        });
        dau1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int valor1 = dice();
                switch (valor1) {
                    case 1:
                        dau1.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dau1.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dau1.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dau1.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dau1.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dau1.setImageResource(R.drawable.dice_6);
                        break;
                }
            }
        });
        dau2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int valor2 = dice();
                switch (valor2) {
                    case 1:
                        dau2.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dau2.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dau2.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dau2.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dau2.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dau2.setImageResource(R.drawable.dice_6);
                        break;
                }
            }
        });
    }

    public static int dice() {
        return RANDOM.nextInt(6) + 1;
    }
}